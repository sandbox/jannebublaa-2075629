<?php
/**
 * @file
 * A default comments wrapper for the bublaa plugin.
 */
?>
<div id="bublaa-comments" data-title="<?php echo $title ?>" data-id="<?php echo $data_id ?>" data-url=""></div>
<script type="text/javascript">
  window.bublaa = {
    config: {
      bubble: "<?php echo $bubble ?>", // name of the bubble (required)
      forumUrl: "<?php echo $url ?>"   // link to your embedded forum example.com/forum
    }
  };
</script>
<noscript><?php echo t("Your browser does not support JavaScript. Enable JavaScript or use a browser that supports it.") ?></noscript>
