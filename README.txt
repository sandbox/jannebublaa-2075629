Bublaa embedded forums & commenting
-----------------------------------

The bublaa module offers variety of widgets to embed to your site. 
You can create embedded bublaa forums, include bublaa commenting on your content pages and also show the bublaa activity feed on your site.


Configure the module
--------------------

The first step is to go to configure the module. 
Go to module cofiguration section or navigate to Configuration > Web services > Bublaa in order to tell drupal some initial settings for the module.

These settings are important essentially if you already have an existing forum at bublaa.com. 
If not, you can leave the default forum name empty at this point and the interface will guide you through creating your first forum.


Bublaa forums
-------------
The bublaa module comes with a new content type called "Bublaa forum".

You can add as many forums as you wish by using this content type. 
Once adding a new bublaa forum, the content form will ask you for the forum name created at bublaa.com. 
By default, the default forum name is already filled in this field that you have provided to the bublaa configuration. 
In case you need to embed multiple forums on your site, we have provided you the possibility to define the forum name page specifically.


Comments and activity feed
--------------------------

Comments and activity feed widgets can be included on your site as drupal blocks. Just navigate to Structure > Blocks and select the correct areas for these widgets.

Both of these blocks will use the default forum set in the bublaa web service configuration as their data source.
