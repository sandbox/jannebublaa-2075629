<?php
/**
 * @file
 * A default forum wrapper for the bublaa plugin.
 */
?>
<div id="bublaa-forum"></div>
<script type="text/javascript">
  window.bublaa = {
    config: {
      bubble: "<?php echo $bubble ?>", // name of the bubble
      noBubbleRoute: "<?php echo $user_can_edit ? 'embeddedCreateNew' : 'embeddedNotFound' ?>"
      <?php if ($user_can_edit) : ?>
      ,bubbleCreatedSuccess: function(name) {
      	// Update the form values and trigger a click event on the submit button in order
      	// to make necessary updates in the backend side. The form's submit button already
      	// has the mousedown attached by drupal, so all we need to do is trigger that.
        var form = document.getElementById('<?php echo $update_form['#id'] ?>');
        var inputs = form.getElementsByTagName('input');
        var submit = null;
        for (var i in inputs) {
          // Update input values and store the submit button
          var inp = inputs[i];
          if (inp.name === 'node_id') {
            inp.value = <?php echo $node_id ?>;
          } else if (inp.name === 'bubble_name') {
            inp.value = name;
          } else if (inp.type === 'submit') {
            submit = inp;
          }
      	}
      	if (submit !== null) {
      	  // Trigger click on the submit element.
      	  // We need few different methods here since the implementatinos vary between different browsers.
      	  if (document.createEvent) {
      	    var event = document.createEvent('MouseEvents');
      	    event.initMouseEvent('mousedown', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
      	    submit.dispatchEvent(event);
      	  } else {
      	    var event = document.createEventObject();
      	    submit.fireEvent('onmousedown', event);
      	  }
      	}
      }
      <?php endif; ?>
    }
  };
</script>
<noscript><?php echo t("Your browser does not support JavaScript. Enable JavaScript or use a browser that supports it.") ?></noscript>

<?php 
if ($user_can_edit) :
  // Add the form for the callback.
  echo drupal_render($update_form);
endif;
?>
