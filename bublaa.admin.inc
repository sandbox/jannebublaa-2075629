<?php

/**
 * @file
 * Admin page callback definitions for the Bublaa module.
 */

/**
 * Page callback: Generates the administration form for the module.
 */
function bublaa_admin_settings() {
  $form = array();

  $form['default_bubble'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default bubble'),
  );

  $form['default_bubble']['bublaa_default_bubble'] = array(
    '#type' => 'textfield',
    '#title' => t('Default bubble name'),
    '#description' => t('The "comments" and "activity" blocks need this as they use it as their data source. This is also used as the default value for the bubble name when creating a new bublaa forum on your site.'),
    '#default_value' => variable_get('bublaa_default_bubble', ''),
  );

  // Get all the nodes of type 'bublaa'
  $node_objs = node_load_multiple(array(), array('type' => 'bublaa'));
  $nodes = array();
  foreach ($node_objs as $node) {
    $nodes[$node->nid] = $node->title;
  }

  // Add the forum node field set.
  $form['forum'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forum node'),
  );
  $form['forum']['forum_node_info'] = array(
    '#markup' => '<p>' . t('You need to define the default forum node on your site in order to tell the bublaa widgets where the forum is located at.') . '</p>'
  );
  if (sizeof($nodes) > 0) {
    $form['forum']['bublaa_forum_node'] = array(
      '#type' => 'select',
      '#title' => t('Default forum node'),
      '#description' => t('Select the node where your forum is located at. By default, this points to the first bublaa forum node you have created.'),
      '#default_value' => variable_get('bublaa_forum_node'),
      '#options' => $nodes
    );
  } else {
  	// Display a link to create the bublaa forum node
    $form['forum']['create_forum_node'] = array(
      '#markup' => '<p>' . t('There are no bublaa forum pages created yet on your site. Please <a href="@create">create your first one</a> and it will automatically be marked as your default forum.', array('@create' => url('node/add/bublaa'))) . '</p>'
    );
  }

  // Display preferences
  $form['preferences'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preferences'),
  );
  $form['preferences']['bublaa_nodelist_comments'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show comment count for node lists'),
    '#default_value' => variable_get('bublaa_nodelist_comments', ''),
  );

  return system_settings_form($form);
}

